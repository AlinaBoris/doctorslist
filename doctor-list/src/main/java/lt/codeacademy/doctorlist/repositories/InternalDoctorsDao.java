package lt.codeacademy.doctorlist.repositories;

import lt.codeacademy.doctorlist.controller.Doctor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class InternalDoctorsDao implements DoctorsDao{
    private List<Doctor> doctors;
    
    public InternalDoctorsDao(){
        this.doctors = createDoctors();
    }
    
    private List<Doctor> createDoctors(){
        Doctor doctor_1 = new Doctor();
        doctor_1.setId(101L);
        doctor_1.setName("Profo");
        doctor_1.setSurname("Profanni");
        doctor_1.setSpecialisation("Magician");
        doctor_1.setLocation("Como Lago");
        
        Doctor doctor_2 = new Doctor();
        doctor_2.setName("Docksheier");
        doctor_2.setSurname("Dockwuhrer");
        doctor_2.setSpecialisation("Nurse");
        doctor_2.setLocation("Berlin");
        
        return doctors;
    }


    @Override
    public Optional<Doctor> getDoctor(Long id) {
        return doctors.stream().
                filter(doctor -> doctor.getId().equals(id))
                .findFirst();
    }

    @Override
    public List<Doctor> getDoctors() {
        return doctors;
    }

    @Override
    public boolean deleteDoctor(Long id) {
        List<Doctor> newDoctors = doctors.stream().
                filter(doctor -> !doctor.getId().equals(id))
                .collect(Collectors.toList());
        
        boolean ifDeleted = newDoctors.size() != doctors.size(); 
        
        return ifDeleted;
    }

    @Override
    public Doctor updateDoctor(Doctor doctor) {
        getDoctor(doctor.getId()).ifPresent(employedDoctor -> {
            employedDoctor.setName(doctor.getName());
            employedDoctor.setSurname(doctor.getSurname());
            employedDoctor.setSpecialisation(doctor.getSpecialisation());
            employedDoctor.setLocation(doctor.getLocation());
        });
        
        return doctor;
    }

    @Override
    public Doctor createDoctor(Doctor doctor) {
        Long id = doctors.stream().mapToLong(Doctor::getId)
                .max().orElse(0L);

        doctor.setId(id+1L);
        doctors.add(doctor);

        return null;
    }
}
