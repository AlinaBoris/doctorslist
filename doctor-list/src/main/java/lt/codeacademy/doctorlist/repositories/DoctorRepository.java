package lt.codeacademy.doctorlist.repositories;

import lt.codeacademy.doctorlist.controller.Doctor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {
}
