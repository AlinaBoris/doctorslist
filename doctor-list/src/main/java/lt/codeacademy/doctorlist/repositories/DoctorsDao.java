package lt.codeacademy.doctorlist.repositories;

import lt.codeacademy.doctorlist.controller.Doctor;

import java.util.List;
import java.util.Optional;

public interface DoctorsDao {
    Optional<Doctor> getDoctor(Long id);

    List<Doctor> getDoctors();

    boolean deleteDoctor(Long id);

    Doctor updateDoctor(Doctor doctor);

    Doctor createDoctor(Doctor doctor);
}
