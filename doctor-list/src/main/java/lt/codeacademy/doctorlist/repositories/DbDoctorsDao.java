package lt.codeacademy.doctorlist.repositories;


import lt.codeacademy.doctorlist.controller.Doctor;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Primary
@Repository
public class DbDoctorsDao implements DoctorsDao {
    private JdbcTemplate jdbcTemplate;

    DbDoctorsDao(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Optional<Doctor> getDoctor(Long id) {
        return Optional.ofNullable(
                jdbcTemplate.queryForObject("SELECT * FROM DOCTORS WHERE DOCTOR_ID = ?",
                        new Object[]{id},
                        (resultSet, i) -> mapToDoctor(resultSet, i)));
    }

    private Doctor mapToDoctor(ResultSet resultSet, int i) throws SQLException {
        Doctor doctor = new Doctor();

        doctor.setId(resultSet.getLong("doctor_id"));
        doctor.setName(resultSet.getString("name"));
        doctor.setSurname(resultSet.getString("surname"));
        doctor.setSpecialisation(resultSet.getString("specialisation"));
        doctor.setLocation(resultSet.getString("location"));

        return doctor;
    }

    @Override
    public List<Doctor> getDoctors() {
        return jdbcTemplate.query("SELECT * FROM DOCTORS;",
                (resultSet, i) -> mapToDoctor(resultSet, i));
    }

    @Override
    public boolean deleteDoctor(Long id) {
        return false;
    }

    @Override
    public Doctor updateDoctor(Doctor doctor) {
        return null;
    }

    @Override
    public Doctor createDoctor(Doctor doctor) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO DOCTORS(name, surname, specialisation, location) VALUES(?,?,?,?)",
                    new String[] {"doctor_id"});
            preparedStatement.setString(1, doctor.getName());
            preparedStatement.setString(2, doctor.getSurname());
            preparedStatement.setString(3, doctor.getSpecialisation());
            preparedStatement.setString(4, doctor.getLocation());

            return preparedStatement;
            }, keyHolder);

        return getDoctor(keyHolder.getKey().longValue()).get();
    }
}
