package lt.codeacademy.doctorlist.services;

import lt.codeacademy.doctorlist.controller.Doctor;
import lt.codeacademy.doctorlist.controller.DoctorNotFoundException;
import lt.codeacademy.doctorlist.repositories.DoctorRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DoctorServices {
    private DoctorRepository DoctorRepository;

    public DoctorServices(DoctorRepository DoctorRepository) {
        this.DoctorRepository = DoctorRepository;
    }

    public Doctor getDoctor(Long id) {
        return DoctorRepository.findById(id)
                .orElseThrow(() -> new DoctorNotFoundException("Doctor with id: " + id + " was not found"));
    }

    public void deleteDoctor(Long id) {
        DoctorRepository.deleteById(id);
    }

    public Doctor createOrUpdateDoctor(Doctor Doctor) {
        return DoctorRepository.save(Doctor);
    }

    public List<Doctor> getAllDoctors() {
        return DoctorRepository.findAll();
    }

}
