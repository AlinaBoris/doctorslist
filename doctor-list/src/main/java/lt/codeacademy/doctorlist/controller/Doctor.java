package lt.codeacademy.doctorlist.controller;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "Doctors")
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "doctor_id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "specialisation")
    private String specialisation;

    @Column(name = "location")
    private String location;
}