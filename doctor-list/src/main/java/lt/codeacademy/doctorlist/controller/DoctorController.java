package lt.codeacademy.doctorlist.controller;

import lt.codeacademy.doctorlist.services.DoctorServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/doctors")
public class DoctorController {

    private DoctorServices doctorServices;

    public DoctorController(DoctorServices doctorServices){
        this.doctorServices = doctorServices;
    }

    @GetMapping("/{id}")
    public String getDoctor(@PathVariable Long id, Model model){
        Doctor doctor = doctorServices.getDoctor(id);
        model.addAttribute("doctor", doctor);

        return "doctorpage";
    }

    @PostMapping("/doctor")
    public String submitDoctor(@ModelAttribute Doctor doctor, Model model){
        Doctor newDoctor = doctorServices.createOrUpdateDoctor(doctor);
        model.addAttribute("doctor", newDoctor);

        return "doctorpage";
    }

    @GetMapping("/doctor")
    public String createDoctorForm(Model model){
        model.addAttribute("doctor", new Doctor());

        return "doctorform";
    }

    @GetMapping("/doctor/{id}")
    public String getUpdateDoctorForm(@PathVariable Long id, Model model){
        Doctor doctor = doctorServices.getDoctor(id);
        model.addAttribute("doctor", doctor);

        return "doctorform";
    }

    @GetMapping
    public String getAllDoctors(Model model){
        List<Doctor> doctors = doctorServices.getAllDoctors();
        model.addAttribute("doctors", doctors);

        return "doctorlist";
    }

    @GetMapping("/doctor/{id}/delete")
    public String deleteDoctor(@PathVariable Long id, Model model){
        doctorServices.deleteDoctor(id);
        List<Doctor> doctors = doctorServices.getAllDoctors();
        model.addAttribute("doctors", doctors);

        return "doctorlist";
    }
}
