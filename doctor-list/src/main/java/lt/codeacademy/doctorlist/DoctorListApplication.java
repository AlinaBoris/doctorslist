package lt.codeacademy.doctorlist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoctorListApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoctorListApplication.class, args);
	}

}
